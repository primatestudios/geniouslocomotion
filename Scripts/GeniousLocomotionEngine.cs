using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeniousLocomotionEngine : AGeniousLocomotionEngine, IAirDodgeable, ITargetable
{
    #region LocomotionConstants
    const float k_AirborneTurnSpeedProportion = 5.4f;
    const float k_JumpAbortSpeed = 10f;
    const float k_MinEnemyDotCoeff = 0.2f;
    const float k_InverseOneEighty = 1f / 180f;
    public const float k_StickingGravityProportion = 0.3f;
    const float k_GroundAcceleration = 20f;
    const float k_GroundDeceleration = 25f;
    #endregion
    #region  AnimatorHashes
    readonly int m_JumpsLeft = Animator.StringToHash("HopsLeft");
    readonly int m_JumpSquatTriggered = Animator.StringToHash("JumpSquatTriggered");
    readonly int m_AirDodged = Animator.StringToHash("DodgedInAir");
    readonly int m_HashTimeoutToIdle = Animator.StringToHash("TimeoutToIdle");
    readonly int m_HashAirborneVerticalSpeed = Animator.StringToHash("AirborneVerticalSpeed");
    readonly int m_HashGrounded = Animator.StringToHash("Grounded");
    readonly int m_HashInputDetected = Animator.StringToHash("InputDetected");
    public readonly int m_HorizontalInput = Animator.StringToHash("HorizontalInput");
    public readonly int m_VerticalInput = Animator.StringToHash("VerticalInput");
    public readonly int m_InputMagnitude = Animator.StringToHash("InputMagnitude");
    #endregion
    #region Serialized Fields
    [SerializeField]
    protected float idleTimeout = 5f;// { get { return 5f; } }  // How long before Ellen starts considering random idles.
    [SerializeField]
    protected float groundedDistance = 0.1f;
    [SerializeField]
    protected int numFramesForSmashedStick = 3;
    [SerializeField] float horizontalSmashUnitScalar;
    [SerializeField] float fallSpeedScalar;
    // public float fallSpeedUnitScalar;
    [SerializeField] float jumpHeightScalar = 5f;
    [SerializeField] float jumpSpeedScalar = 1.5f;
    [SerializeField] float groundedDecellerationScalar = 10f;
    [SerializeField] float aerialDecellerationScalar = 10f;
    [SerializeField] int numberOfGroundedRays = 4;
    #endregion
    public override Type RewiredInputTypeDependency { get { return typeof(SmashInputManagerActions); } }

    public override bool EngineInputDetected
    {
        get
        {
            return IsMoveInput;
        }
    }

    public bool IsMoveInput
    {
        get { return !Mathf.Approximately(MoveInput.sqrMagnitude, 0f); }// || isMovingForward; }
    }

    public Vector2 MoveInput
    {
        get
        {
            Vector2 ret = fc.playerInput.GetVector2(SmashInputManagerActions.MovementHorizontal, SmashInputManagerActions.MovementVertical);
            if (!GeniousSettings.instance.allowZMovement)
            {
                ret.y = 0f;
            }
            return ret;
        }
    }
    public LayerMask groundLayer;

    protected float m_IdleTimer;                   // Used to count up to Ellen considering a random idle.
    public float m_ForwardSpeed {get; protected set;}                // How fast Ellen is currently going along the ground.
    public override float m_VerticalSpeed { get { return _vertSpeed; } set { _vertSpeed = value; } }// Debug.Log("Setting VertSpeed to " + m_VerticalSpeed); } }              // How fast Ellen is currently moving up or down.
    protected float _vertSpeed;
    protected bool shortHop = false;
    protected int? jumpInputFrame;
    protected int jumpFrame;
    protected bool squatting = false;
    protected Collider[] m_OverlapResult = new Collider[8];    // Used to cache colliders that are near Ellen.
    protected float m_AngleDiff;                   // Angle in degrees between Ellen's current rotation and her target rotation.
    public Quaternion m_TargetRotation {get; protected set;}         // What rotation Ellen is aiming to have based on input.

    public float gravity
    {
        get
        {
            if (fighterData.attributes.fall_speed == 0)
            {
                Debug.LogError("Error: Frame data isn't properly instantiated");
            }
            return (float)fighterData.attributes.fall_speed * fallSpeedScalar;
        }
    }

    public bool isMovingForwardAutomatically { get; protected set; }

    bool smashedStick { get
        {
            return movementInputMagnitude >= 1 && frameInputMagnitudeWas0 + numFramesForSmashedStick >= GeniousSettings.fixedFrame;
        }
    }

    void OnMoveForwardAutomatically(bool val)
    {
        isMovingForwardAutomatically = val;
    }

    public override void UpdateGroundedState()
    {
        fc.m_IsGrounded = fc.m_CharCtrl.isGrounded;
        if (!fc.m_IsGrounded)
        {
            // After the movement store whether or not the character controller is grounded.
            for(int i = 0; i < numberOfGroundedRays; i++)
            {
                var rotation = 360f / numberOfGroundedRays * i;
                Vector3 direction = Vector3.Normalize(Quaternion.Euler(0, rotation, 0f) * transform.forward) * fc.m_CharCtrl.radius;
                Vector3 origin = transform.position + fc.m_CharCtrl.center + direction;
                Ray ray = new Ray(origin, -Vector3.up);
                float rayLength = fc.m_CharCtrl.height / 2f + groundedDistance;
                RaycastHit[] hits = Physics.RaycastAll(ray, rayLength, Physics.AllLayers, QueryTriggerInteraction.Ignore);
                //RaycastHit closestHit = new RaycastHit();
                //float closestDistance = float.MaxValue;
                int numHitSelf = 0;
                foreach (var hit in hits)
                {
                    //make sure layer isn't hitting itself
                    if (hit.transform != transform && !hit.transform.IsChildOf(transform))
                    {
                        if (groundLayer.Contains(hit.transform.gameObject.layer))
                        {
                            fc.m_IsGrounded = true;
                            break;
                        }
                    }
                    else
                    {
                        numHitSelf++;
                    }
                }

                if (fc.m_IsGrounded)
                {
                    break;
                }
            }

            /*
                    if (hit.distance < closestDistance)
                    {
                        closestHit = hit;
                        closestDistance = hit.distance;
                    }
                }
            }

            if (closestHit.transform != null)
            {
                rayHitTheGround = true;
            }
            else
            {
                Debug.Log("No Closest Hit!");
            }
            */
        }
        // If Ellen is not on the ground then send the vertical speed to the animator.
        // This is so the vertical speed is kept when landing so the correct landing animation is played.
        // if (!fc.m_IsGrounded)
        fc.m_Animator.SetFloat(m_HashAirborneVerticalSpeed, fc.m_IsGrounded ? 0f : m_VerticalSpeed);

        // Send whether or not Ellen is on the ground to the animator.
        fc.m_Animator.SetBool(m_HashGrounded, fc.m_IsGrounded);
    }

    public override void SetMinFallSpeed()
    {
        m_VerticalSpeed = -gravity;// minFallSpeed;
    }
    public override void UpdateInputAnimatorValues(Animator m_Animator){
        if (fc.m_IsGrounded)
        {
            m_Animator.SetInteger(m_JumpsLeft, fighterData.attributes.jumps);
            m_Animator.SetBool(m_AirDodged, false);
        }

        m_Animator.SetFloat(m_HorizontalInput, MoveInput.x);
        m_Animator.SetFloat(m_VerticalInput, MoveInput.y);
        movementInputMagnitude = fc.playerInput.GetInputMagnitude(MoveInput);
        m_Animator.SetFloat(m_InputMagnitude, movementInputMagnitude);// MoveInput.magnitude);

    }

    public void TriggerAirDodge()
    {
        fc.m_Animator.SetBool(m_AirDodged, true);
    }

    bool lockedOn { get
        {
            bool targetButtonDown = fc.playerInput.Get<bool>(SmashInputManagerActions.Target);
            return autoTarget || (targetButtonDown && !autoTarget) || (autoTarget && !targetButtonDown);
        } }
    public ADamageable currentTarget {get; protected set;}
    public override void SetTargetRotation()
    {
        if (canRotate)
        {
            SetBaseTargetRotation();

            bool shouldSetRotaion = true;
            if (lockedOn)
            {
                if (currentTarget == null)
                {
                    SetNextTarget();
                }
                if (currentTarget != null)
                {
                    shouldSetRotaion = false;
                    Vector3 toLook = currentTarget.transform.position - transform.position;
                    if (toLook != Vector3.zero)
                    {
                        transform.rotation = Quaternion.LookRotation(currentTarget.transform.position - transform.position);// * Quaternion.Euler(90, 90, 90);
                    }

                    transform.SetXLocalRotation(0f); //always look eye! (forward)
                }
                else
                {
                    // Debug.Log("No targets found.");
                }
            }
            else
            {
                currentTarget = null;
                previouslySelectedTargets.Clear();
            }

           // m_TargetRotation *= Quaternion.Euler(90, 90, 90);// Vector2.right * 90);
            if (shouldSetRotaion && !isMovingForwardAutomatically && Vector2.zero != MoveInput)
            {
                transform.rotation = m_TargetRotation;
            }
        }
    }

    public virtual void SetBaseTargetRotation()
    {
        // Create three variables, move input local to the player, flattened forward direction of the camera and a local target rotation.
        Vector2 moveInput = MoveInput;
        Vector3 localMovementDirection = new Vector3(moveInput.x, 0f, moveInput.y).normalized;

        Vector3 forward = Camera.main.transform.forward;//Quaternion.Euler(0f, cameraSettings.Current.m_XAxis.Value, 0f) * Vector3.right;
        forward.y = 0f;
        forward.Normalize();

        Quaternion targetRotation;
        
        // If the local movement direction is the opposite of forward then the target rotation should be towards the camera.
        if (Mathf.Approximately(Vector3.Dot(localMovementDirection, Vector3.forward), -1.0f))
        {
            targetRotation = Quaternion.LookRotation(-forward);
        }
        else
        {
            // Otherwise the rotation should be the offset of the input from the camera's forward.
            Quaternion cameraToInputOffset = Quaternion.FromToRotation(Vector3.forward, localMovementDirection);
            targetRotation = Quaternion.LookRotation(cameraToInputOffset * forward);
        }

        // The desired forward direction of Ellen.
        Vector3 resultingForward = targetRotation * Vector3.forward;
        resultingForward = Quaternion.LookRotation(resultingForward, Vector3.back).eulerAngles;
        // If attacking try to orient to close enemies.
        if (fc.m_InAttack)
        {
            // Find all the enemies in the local area.
            Vector3 centre = transform.position + transform.forward * 2.0f + transform.up;
            Vector3 halfExtents = new Vector3(3.0f, 1.0f, 2.0f);
            int layerMask = 1 << LayerMask.NameToLayer("Enemy");
            int count = Physics.OverlapBoxNonAlloc(centre, halfExtents, m_OverlapResult, targetRotation, layerMask);

            // Go through all the enemies in the local area...
            float closestDot = 0.0f;
            Vector3 closestForward = Vector3.zero;
            int closest = -1;

            for (int i = 0; i < count; ++i)
            {
                // ... and for each get a vector from the player to the enemy.
                Vector3 playerToEnemy = m_OverlapResult[i].transform.position - transform.position;
                playerToEnemy.y = 0;
                playerToEnemy.Normalize();

                // Find the dot product between the direction the player wants to go and the direction to the enemy.
                // This will be larger the closer to Ellen's desired direction the direction to the enemy is.
                float d = Vector3.Dot(resultingForward, playerToEnemy);

                // Store the closest enemy.
                if (d > k_MinEnemyDotCoeff && d > closestDot)
                {
                    closestForward = playerToEnemy;
                    closestDot = d;
                    closest = i;
                }
            }

            // If there is a close enemy...
            if (closest != -1)
            {
                // The desired forward is the direction to the closest enemy.
                resultingForward = closestForward;
                
                // We also directly set the rotation, as we want snappy fight and orientation isn't updated in the UpdateOrientation function during an atatck.
                transform.rotation = Quaternion.LookRotation(resultingForward);
            }
        }

        // Find the difference between the current rotation of the player and the desired rotation of the player in radians.
        float angleCurrent = Mathf.Atan2(transform.forward.x, transform.forward.z) * Mathf.Rad2Deg;
        float targetAngle = Mathf.Atan2(resultingForward.x, resultingForward.z) * Mathf.Rad2Deg;

        m_AngleDiff = Mathf.DeltaAngle(angleCurrent, targetAngle);
        m_TargetRotation = targetRotation;
        //m_TargetRotation *= Quaternion.Euler(Vector2.up * 90);

        //            }
    }
    
    bool jumpTriggered;
    public override void TriggerJump(bool bounce = false)
    {
        jumpTriggered = true;
        jumpFrame = GeniousSettings.fixedFrame;
        if (bounce)
        {
            //   m_VerticalSpeed = jumpSpeed;
        }
        GenericCoroutineManager.instance.RunAfterFrame(() =>
        {
            fc.m_Animator.SetInteger(m_JumpsLeft, fc.m_Animator.GetInteger(m_JumpsLeft) - 1);
        });
    }

    void SetVerticalSpeed(bool? overrideRising = null)
    {
        float x = GeniousSettings.fixedFrame - jumpFrame + 1;
        bool rising = overrideRising.HasValue ? overrideRising.Value : m_VerticalSpeed > 0;
        float lastFrameDisplacement = GetVerticalDisplacement(x - 1, rising);
        float thisFrameDisplacement = GetVerticalDisplacement(x, rising);
        if (m_VerticalSpeed > 0 && thisFrameDisplacement - lastFrameDisplacement <= 0)
        {
            thisFrameDisplacement = GetVerticalDisplacement(x, false);
        }

        m_VerticalSpeed = thisFrameDisplacement - lastFrameDisplacement;
        m_VerticalSpeed *= jumpHeightScalar;
    }

    float GetVerticalDisplacement(float x, bool rising)
    {
        float ret;
        int lastFrame = shortHop ? fighterData.attributes.sh_air_time : fighterData.attributes.fh_air_time;
        lastFrame = Mathf.RoundToInt((float)lastFrame / jumpSpeedScalar);
        float height = (float)(shortHop ? fighterData.attributes.hop_height : fighterData.attributes.jump_height);
        
        var fallSpeed = fighterData.attributes.fall_speed;
        float apexFrame = lastFrame - (height / fallSpeed); 

        float a = GetJumpCoefficient(rising, height, apexFrame, lastFrame);
        ret = CalculateVertexFormQuadratic(a, x, apexFrame, height);
        return ret;
    }

    float CalculateVertexFormQuadratic(float a, float x, float h, float k)
    {
        var ret = a * Mathf.Pow(x - h, 2) + k;
        // Debug.Log(ret + " = " + a + "(" + x + " - " + h + ")^2 + " + k);
        return ret;
    }

    float GetJumpCoefficient(bool rising, float jumpHeight, float apexFrame, int lastFrame)
    {
        float ret;
        if (rising)
        {
            ret = -jumpHeight / Mathf.Pow(apexFrame, 2);
            // Debug.Log("RisingA = " + ret + " = -" + jumpHeight + " / " + apexFrame + "^2");
        }
        else
        {
            ret = -jumpHeight / Mathf.Pow((lastFrame - apexFrame), 2);
            // Debug.Log("FallingA = " + ret + " = -" + jumpHeight + " / (" + lastFrame + "-" + apexFrame + ")^2");
        }

        return ret;
    }

    public override void OnJump(bool val)
    {
                //Debug.Log("OnJump!");
      //  if (fc.state != FighterController.FighterStateEnum.Normal)
        if (!canJump)//(fc.s is Sm4shFighterStates.Normal))
        {
            return;
        }
        if (fc.m_IsGrounded && val && !jumpInputFrame.HasValue)
        {
            squatting = true;
            fc.m_Animator.SetTrigger(m_JumpSquatTriggered);
            jumpInputFrame = GeniousSettings.fixedFrame;
            GenericCoroutineManager.instance.RunInFixedUpdateFrames(fighterData.attributes.jumpsquat, () =>
            {
                fc.m_Animator.ResetTrigger(m_JumpSquatTriggered);
                squatting = false;
                shortHop = !jumpInputFrame.HasValue;
                TriggerJump(true);
            });
        }

        if (!val)
        {
            jumpInputFrame = null;
        }
    }

    protected override void FixedUpdate()
    {
        TimeoutToIdle();
        GenericCoroutineManager.instance.RunAfterFrame(() => { VerifyZMovement(); });
    }

    Vector3 pointOntheLine;
    bool wasAllowingZMovement;
    /// <summary>
    /// If we disallow Z movement, make sure they are staying on the line
    /// </summary>
    void VerifyZMovement()
    {
        if (!GeniousSettings.instance.allowZMovement)
        {
            var currentOrientationXDirectionVector = Camera.main.transform.right;
            if (wasAllowingZMovement)
            {
                //https://bobobobo.wordpress.com/2008/01/07/solving-linear-equations-ax-by-c-0/
                pointOntheLine = transform.position;
            }
            transform.position = CalculateClosestPositionOnTheLine(transform.position, currentOrientationXDirectionVector, pointOntheLine);
        }

        wasAllowingZMovement = GeniousSettings.instance.allowZMovement;
    }

    //Used these videos for reference
    //https://www.youtube.com/watch?v=l-_DbsFjz_s
    //https://www.youtube.com/watch?v=0lG53-ogF2k
    Vector3 CalculateClosestPositionOnTheLine(Vector3 incoming, Vector3 direction, Vector3 startPoint)
    {
        float x = incoming.x;
        float y = incoming.z;
        Vector2 dir = new Vector2(direction.x, direction.z);
        Vector2 start = new Vector2(startPoint.x, startPoint.z);

        float lambda = -((dir.x * (start.x - x)) + (dir.y * (start.y - y)));
        lambda /= ((dir.x * dir.x) + (dir.y * dir.y));

        Vector2 ret = start + lambda * dir;
        //Debug.Log("RET: " + ret + " = " + start + " + " + lambda + " * " + dir);
        return new Vector3(ret.x, incoming.y, ret.y);
    }


    void TimeoutToIdle()
    {
        if (fc.m_IsGrounded && !fc.InputDetected)
        {
            m_IdleTimer += Time.deltaTime;

            if (m_IdleTimer >= idleTimeout)
            {
                m_IdleTimer = 0f;
                fc.m_Animator.SetTrigger(m_HashTimeoutToIdle);
            }
        }
        else
        {
            m_IdleTimer = 0f;
           fc. m_Animator.ResetTrigger(m_HashTimeoutToIdle);
        }

        fc.m_Animator.SetBool(m_HashInputDetected, fc.InputDetected);
    }

    protected override void Start()
    {
        base.Start();
        SetupTargetsCollider();
    }

    public bool autoTarget { get; protected set; }
    void OnTargetingLockChanged(bool val)
    {
        if (val)
        {
            autoTarget = !autoTarget;
        }
    }

    void OnNextTarget(bool val)
    {
        if (val && lockedOn)
        {
            SetNextTarget();
        }
    }

    void OnPreviousTarget(bool val)
    {
        if (val && lockedOn)
        {

        }
    }

    public override void RegisterInputCallbacks(PlayerInput fighterInput)
    {
        fighterInput.AppendAction<bool>(SmashInputManagerActions.MoveForward, OnMoveForwardAutomatically);
        fighterInput.AppendAction<bool>(SmashInputManagerActions.Jump, OnJump);
        fighterInput.AppendAction<bool>(SmashInputManagerActions.ToggleTargetLock, OnTargetingLockChanged);
        fighterInput.AppendAction<bool>(SmashInputManagerActions.NextTarget, OnNextTarget);
        fighterInput.AppendAction<bool>(SmashInputManagerActions.PreviousTarget, OnPreviousTarget);
        #region CAMERA_WORK
        //This could maybe go into its own Camera Engine?
        fighterInput.AppendAction<Vector2>(SmashInputManagerActions.DpadHorizontal, CameraChanged, SmashInputManagerActions.DpadVertical);
        fighterInput.AppendAction<bool>(SmashInputManagerActions.ToggleCamera, ToggleCamera);
    }

    void ToggleCamera(bool val)
    {
        if (val)
        {
            VirtualCameraManager.instance.Next();
        }
    }

    void CameraChanged(Vector2 dpad)
    {
        if (dpad.y < 0)
        {
            CameraDown();
        }
        else if (dpad.y > 0)
        {
            CameraUp();
        }

        else if (dpad.x < 0)
        {
            CameraLeft();
        }
        else if (dpad.x > 0)
        {
            CameraRight();
        }
    }

    void CameraDown()//bool val)
    {
        VirtualCameraManager.instance.SetCameraIndex(3);
    }

    void CameraUp()//bool val)
    {
        VirtualCameraManager.instance.SetCameraIndex(0);
    }

    void CameraLeft()//bool val)
    {
        VirtualCameraManager.instance.SetCameraIndex(1);
    }

    void CameraRight()//bool val)
    {
        VirtualCameraManager.instance.SetCameraIndex(2);
    }
#endregion

    public override void MoveCharacter(Vector3 motion)
    {
        fc.m_CharCtrl.Move(motion);
    }

    public void MoveCharacterMaintainOrientation(Vector3 motion)
    {
        var startRotation = transform.rotation;
        if (!isMovingForwardAutomatically)
        {
            transform.rotation = m_TargetRotation;
        }
//        MoveDirect(transform.up * movement.y);
        MoveCharacter(motion);
        transform.rotation = startRotation;
    }

    /*Vector3 GetFixedDeltaTimeTranslation(float scalar, float? forwardSpeed = null)
    {
        if (forwardSpeed == null)
            forwardSpeed = m_ForwardSpeed;

        //        return -transform.forward * forwardSpeed.Value * Time.fixedDeltaTime * scalar;
        return transform.forward * forwardSpeed.Value * Time.fixedDeltaTime * scalar;
    }*/

     public override void OnAnimatorMove()
    {
        base.OnAnimatorMove();
        if (!fc.enabled || fc.animationMovementOffset.y > 0)
            return;
        /*Vector3 euler = m_Animator.bodyRotation.normalized.eulerAngles;
        m_Animator.bodyRotation = Quaternion.Euler(euler.x + 90, euler.y, euler.z);
        */
        Vector3 movement = Vector3.zero;

        // If Ellen is on the ground...
        if (fc.m_IsGrounded)
        {
            // ... raycast into the ground...
            RaycastHit hit;
            Ray ray = new Ray(transform.position + Vector3.up * groundedDistance * 0.5f, -Vector3.up);
            if (Physics.Raycast(ray, out hit, groundedDistance, Physics.AllLayers, QueryTriggerInteraction.Ignore))
            {
                // ... and get the movement of the root motion rotated to lie along the plane of the ground.
                movement = Vector3.ProjectOnPlane(fc.m_Animator.deltaPosition, hit.normal);
                
                // Also store the current walking surface so the correct audio is played.
                Renderer groundRenderer = hit.collider.GetComponentInChildren<Renderer>();
                m_CurrentWalkingSurface = groundRenderer ? groundRenderer.sharedMaterial : null;
            }
            else
            {
                // If no ground is hit just get the movement as the root motion.
                // Theoretically this should rarely happen as when grounded the ray should always hit.
                movement = fc.m_Animator.deltaPosition;
                m_CurrentWalkingSurface = null;
            }
        }
        else
        {
            // If not grounded the movement is just in the forward direction.
            movement = GetAirMovement(movement);
        }

        // Rotate the transform of the character controller by the animation's root rotation.
        fc.m_CharCtrl.transform.rotation *= fc.m_Animator.deltaRotation;

        // Add to the movement with the calculated vertical speed.
        movement += m_VerticalSpeed * Vector3.up * Time.deltaTime;

        MoveCharacterFromAnimator(movement);
        //UpdateGroundedState();
    }


    //TODO - is this unnecesary?
    protected Vector3 GetAirMovement(Vector3 movement)
    {
        //Handled elsewhere
        return movement;
    }

    protected void MoveCharacterFromAnimator(Vector3 movement)
    {
       fc.m_CharCtrl.Move(movement);
    }

    /*public override void HandleMoveFromInput()
    {
        if (fc.IsMoveInput)
        {
            if (fc.canMove)
            {
                //CalculateXZMovement();
            }
        }
    }*/

    public override void CalculateMovement()
    {
        CalculateVerticalMovement();
        CalculateXZMovement();
        UpdateGroundedState();
    }

    void CalculateVerticalMovement()
    {
        //Maybe it should be [fc.animationMovementOffset.y != 0] ?
        if (fc.ignoreRootMotionOnThisFrame != GeniousSettings.fixedFrame && fc.animationMovementOffset.y > 0)
        {
            m_VerticalSpeed = 0f;
            return;
        }
        if (fc.m_IsGrounded && !jumpTriggered)
        {
            // When grounded we apply a slight negative vertical speed to make Ellen "stick" to the ground.
            m_VerticalSpeed = -k_StickingGravityProportion;// -gravity * k_StickingGravityProportion;
        }
        else if (jumpTriggered)
        {
            SetVerticalSpeed(true);
        }
        else
        {
            if (!fc.inKnockback)
            {
                if (jumpFrame <= 0)
                {
                    SetMinFallSpeed();
                }
                else
                {
                    SetVerticalSpeed();
                }
            }

            m_VerticalSpeed = Mathf.Max(m_VerticalSpeed, -gravity);
        }

        jumpTriggered = false;
        MoveCharacterMaintainOrientation(new Vector3(0f, m_VerticalSpeed * Time.fixedDeltaTime * fallSpeedScalar, 0f));
    }

    Vector2 lastMoveInput;
    float lastInputMagnitude;
    int frameInputMagnitudeWas0 = int.MinValue;
    bool wasGrounded;
    bool running = false;
    void CalculateXZMovement()
    {
        if (!canMove)
        {
            return;
        }
       // float scalar = fc.m_IsGrounded ? horizontalSmashUnitScalar :  1f;
        if (movementInputMagnitude > 0)
        {
            m_ForwardSpeed += fighterData.attributes.run_acceleration;
            if (fc.m_IsGrounded)
            {
                if (running)
                {
                    if (movementInputMagnitude != 1)
                    {
                        SkidToAStop();
                    }
                    else
                    {
                        m_ForwardSpeed = Mathf.Min(m_ForwardSpeed, fighterData.attributes.run_speed);
                    }
                }
                else if (smashedStick)
                {
                    running = true; //start running
                    m_ForwardSpeed = fighterData.attributes.initial_dash;
                }
                else
                {
                    m_ForwardSpeed = Mathf.Min(m_ForwardSpeed, fighterData.attributes.walk_speed * movementInputMagnitude);
                }
            }
            else
            {
                // scalar = airMovementScalar;
                m_ForwardSpeed += fighterData.attributes.air_acceleration;
                m_ForwardSpeed = Mathf.Min(m_ForwardSpeed, fighterData.attributes.air_speed);
            }
        }
        else //Stopped
        {
            if (fc.m_IsGrounded)
            {
                if (!wasGrounded || running)
                {
                    SkidToAStop();
                }
                else
                {
                    m_ForwardSpeed -= (fighterData.attributes.run_deceleration * groundedDecellerationScalar);
                }
            }
            else
            {
                m_ForwardSpeed -= (fighterData.attributes.air_friction * aerialDecellerationScalar);
            }

            m_ForwardSpeed = Mathf.Max(0f, m_ForwardSpeed);
        }

        lastMoveInput = MoveInput;
        lastInputMagnitude = movementInputMagnitude;
        if (lastInputMagnitude <= 0)
        {
            frameInputMagnitudeWas0 = GeniousSettings.fixedFrame;
        }
        wasGrounded = fc.m_IsGrounded;

        //Move normally regardless of the direction your facing
        var ogRotation = transform.rotation;
        transform.rotation = m_TargetRotation;
        MoveCharacterMaintainOrientation(transform.forward * m_ForwardSpeed * Time.fixedDeltaTime * horizontalSmashUnitScalar);
        transform.rotation = ogRotation;
    }

    void SkidToAStop()
    {
        running = false;
        m_ForwardSpeed = 0f;
    }

    HashSet<ADamageable> previouslySelectedTargets = new HashSet<ADamageable>();
    int targetIndex = 0;
    void SetNextTarget(bool lastWasThisCharacter = false)
    {
        var possibleTarget = targetsCollider.GetTargetAtIndexByDistance<ADamageable>(targetIndex);
        if (possibleTarget == null)
        {
            if (targetIndex == 0 || lastWasThisCharacter)
            {
                currentTarget = null;
                return;
            }
            targetIndex = 0;
            previouslySelectedTargets.Clear();
            SetNextTarget();
            return;
        }

        targetIndex++;
        if (possibleTarget == fc.m_Damageable)
        {
            SetNextTarget(true);
            return;
        }

        if (previouslySelectedTargets.Contains(possibleTarget))
        {
            SetNextTarget();
            return;
        }

        previouslySelectedTargets.Add(possibleTarget);
        currentTarget = possibleTarget;
    }

    [SerializeField] Vector3 targetableRange = Vector3.one * 25;
    TargetSphereCollider targetsCollider;
    void SetupTargetsCollider()
    {
        targetsCollider = TargetSphereCollider.Create<ADamageable>(targetableRange);
        var go = targetsCollider.gameObject;
        go.transform.SetParent(transform);
        go.transform.localPosition = Vector3.zero;
        go.transform.localEulerAngles = Vector3.zero;
        go.transform.localScale = Vector3.one;
    }
}